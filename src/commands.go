package main

/*
 * Bot commands
 * %command <arg>
 */

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"

	"github.com/StefanoBelli/bot"
)

const (
	SHELL string = "/bin/bash"
)

func execCmd(cmd *bot.Cmd) (string, error) {
	completeCommand := cmd.RawArgs

	out, err := exec.Command(SHELL, "-c", completeCommand).CombinedOutput()

	finalOut := fmt.Sprintf("--[OK]--> %s\n=====\n%s\n=====\n", completeCommand, string(out))

	finalErr := fmt.Sprintf("--[FAIL]--> Not working: %s !\n=====\n%s\n=====\n", completeCommand, string(out))

	if err == nil {
		return finalOut, nil
	} else {
		return finalErr, nil
	}
}

func chcwdCmd(cmd *bot.Cmd) (string, error) {
	completePath := cmd.RawArgs
	err := os.Chdir(completePath)

	if err == nil {
		return fmt.Sprintf("--[OK]--> Changed CWD: %s\n", completePath), nil
	} else {
		return fmt.Sprintf("--[FAIL]--> Failed to change CWD: %s (Error: %s)\n", completePath, err), nil
	}
}

func listDirCmd(cmd *bot.Cmd) (string, error) {
	completePath := cmd.RawArgs
	if completePath == "" {
		completePath = "."
	}

	files, err := ioutil.ReadDir(completePath)
	dirs := ""

	for _, ls := range files {
		dirs = dirs + " , " + ls.Name()
	}

	finalOut := fmt.Sprintf("--[OK]--\n%s ", dirs)
	finalErr := fmt.Sprintf("--[FAIL]--> Failed to list directory!\n")

	if err == nil {
		return finalOut, nil
	} else {
		return finalErr, nil
	}
}
