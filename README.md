## PeppinoBot

A simple *IRC* bot written in Golang

this bot lets you able to control your computer (bot host) remotely.

### Build
You need to install my fork of: 
[go-chat-bot/bot](https://github.com/go-chat-bot/bot)

My fork:
[StefanoBelli/bot](https://github.com/StefanoBelli/bot)


 * Set GOPATH environment var.
 ~~~
 export GOPATH=$HOME/.gopackages
 ~~~

 * Get package 
 ~~~
 go get -u github.com/StefanoBelli/bot
 ~~~

 * Build peppino
 ~~~
 cd peppino-ircbot/src
 go build * .go
 ~~~

 * Run peppino
 ~~~
 ./bot
 ~~~

## IRC Commands
 * exec (run command)
 ~~~
 %exec cmd
 ~~~

 * lsdir (lists dir)
 ~~~
 %lsdir dir # if empty lists .
 ~~~

 * chdir (changes cwd)
 ~~~
 %chdir dir
 ~~~


